<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-24
 * Time: 오후 5:19
 */

namespace App\DataStructure\Map;

use App\DataStructure\AbstractCollection;
use App\DataStructure\Generatable;

abstract class AbstractMap extends AbstractCollection implements MapInterface, Generatable
{

    const TYPE = 'map';

    protected $values = array();

    abstract public function put($key, $value);

    abstract public function putAll($array, $merge = true);

    abstract public function get($key);

    abstract public function remove($key);

    public function keys()
    {
        return new \ArrayIterator(array_keys($this->values));
    }

    public function values()
    {
        return new \ArrayIterator(array_values($this->values));
    }


    public function generator(\Closure $callback = null)
    {
        foreach($terator = $this as $value)
        {
            if($callback === null)
            {
                yield $value;

            }else {

                yield $callback($value);
            }
        }
    }

    public function getCachingIterator($flags = \CachingIterator::CALL_TOSTRING)
    {
        return new \CachingIterator($this, $flags);
    }



}