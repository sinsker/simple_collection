<?php

namespace App\DataStructure\Map;

use App\DataStructure\Collection;

interface MapInterface extends Collection
{

    public function put($key, $value);

    public function putAll($array, $merge = true);

    public function get($key);

    public function remove($key);



}