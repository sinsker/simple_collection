<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-24
 * Time: 오후 2:59
 */

namespace App\DataStructure;

use App\DataStructure\Collection;

abstract class AbstractCollection implements Collection, \ArrayAccess, \Countable, \JsonSerializable
{

    const TYPE = 'collection';

    protected $values = array();

    protected $size = 0;




    public function toArray()
    {
        return (array)$this->values;
    }

    public function count()
    {
        return $this->size;
    }

    public function equals($object)
    {
        return ( $this->size() === count($object) && array_diff($this->values, $object) === array_diff($object,$this->values));
    }

    public function clear()
    {
        $this->rewind();
    }

    public function isEmpty()
    {
        return empty($this->values);
    }

    public function clone()
    {
        return new static($this->values);
    }


    /********************** Iterator ***********************/

    public function rewind()
    {
        reset($this->values);
    }

    public function key()
    {
        return key($this->values);
    }

    public function valid()
    {
        $key = $this->key();

        return ($key !== null && $key !== false);
    }

    public function next()
    {
        return next($this->values);
    }

    public function current()
    {
        return current($this->values);
    }

    /********************** END Iterator ***********************/



    public function offsetExists($offset)
    {
        return isset($this->values[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->values[$offset]) ? $this->values[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->values[] = $value;
        } else {
            $this->values[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->values[$offset]);
    }


    function jsonSerialize()
    {
        return $this->toArray();
    }

    function type()
    {
        return static::TYPE;
    }



}