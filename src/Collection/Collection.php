<?php

namespace App\DataStructure;


interface Collection extends \Iterator
{

    public function get($key);

    public function remove($key);

    public function clear();

    public function clone();

    public function equals($object);

    public function toArray();

    public function isEmpty();

}