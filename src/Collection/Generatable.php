<?php
/**
 * Created by PhpStorm.
 * User: SIEE
 * Date: 2017-03-24
 * Time: 오후 11:01
 */

namespace App\DataStructure;


interface Generatable
{

    public function generator(\Closure $callback = null);

}