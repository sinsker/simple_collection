<?php

namespace App\DataStructure\Map;


class Map extends AbstractMap
{
    protected $values = array();

    public function __construct($key = '', $value = '')
    {
        if(!empty($key) && !empty($value))
        {
            $this->put($key, $value);

        }

        if( count(func_get_args()) == 1)
        {
            $this->putAll($key, false);
        }

    }

    public function put($key, $value)
    {
        $this->values[$key] = $value;

        $this->size++;
    }

    public function putAll($array, $merge = true)
    {
        $tmp = [];

        if( $array instanceof self )
        {
            $tmp = $array->toArray();

        }else if(!is_array($array))
        {
            $tmp[0] = $array;
        }

        if($merge === true)
        {
            array_merge($this->values, $tmp);

        }else{
            $this->values = $tmp;
        }

        $this->size = count($this->values);

    }

    public function get($key)
    {
        if(empty($this->values[$key]))
        {
            return false;
        }

        return  $this->values[$key];
    }

    public function remove($key)
    {
        unset($this->values[$key]);

        $this->size--;
    }

}